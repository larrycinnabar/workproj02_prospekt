"use strict";

$(function () {
    // File is converted from ES6 syntax
    //
    var App = function () {
        return {
            init: function init() {
                ExamplesCarousel.init();
                ImageTitles.init();
                Calculator.init();
                Gallery.init();
                Nav.init();

                //DummyModule.init();
            }
        };
    }()

    /**
     * Examples Carousel
     */
    ,
        ExamplesCarousel = function () {
        var $carousel = $('.examples__wrapper');
        return {
            init: function init() {
                if (!$carousel.length) return false;

                $carousel.slick({
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    prevArrow: $(".examples__arrow--left"),
                    nextArrow: $(".examples__arrow--right")
                });

                var timer;
                $carousel.on("mouseenter", function () {
                    $(".examples__arrow-lg").addClass('active');
                    window.clearTimeout(timer);
                }).on("mouseleave", function () {
                    timer = window.setTimeout(function () {
                        $(".examples__arrow-lg").removeClass('active');
                    }, 1000);
                });

                $(".examples__arrow-lg").on('mouseenter', function () {
                    window.clearTimeout(timer);
                });

                $(".examples__arrow-lg--left").on('click', function () {
                    $(".examples__arrow--left").click();
                    return false;
                });
                $(".examples__arrow-lg--right").on('click', function () {
                    $(".examples__arrow--right").click();
                    return false;
                });
            }
        };
    }()

    /**
     * Calculator for prices
     */
    ,
        Calculator = function () {
        var $calc = $('.calculator');
        return {
            init: function init() {
                if (!$calc.length) return false;

                var $btn = $calc.find(".calculator__submit");
                $btn.on("click", function () {

                    $(".calculator__results").show();

                    // gather data
                    var square = Number($(".calc-input-square").val().replace(',', '.'));
                    var height = Number($(".calc-input-height").val().replace(',', '.'));
                    var level = Number($(".calc-input-level").val());
                    var type = Number($(".calc-input-type").val());
                    var location = Number($(".calc-input-location").val());
                    console.log(height);

                    // calculate
                    var v1 = 4000 * height * level * type * location;
                    var v2 = v1 * 0.15;
                    var v3 = v1 * 0.4;
                    var v4 = v1 + v2 + v3;
                    var v5 = v1 * square;
                    var v6 = (v2 + v3) * square;
                    var v7 = v4 * square;

                    var filter = function filter(v) {
                        return v.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1 ');
                    };

                    // show result
                    $(".val-price1").find('strong').text(filter(v1) + " руб.");
                    $(".val-price2").find('strong').text(filter(v2) + " руб.");
                    $(".val-price3").find('strong').text(filter(v3) + " руб.");
                    $(".val-price4").find('strong').text(filter(v4) + " руб.");
                    $(".val-price5").find('strong').text(filter(v5) + " руб.");
                    $(".val-price6").find('strong').text(filter(v6) + " руб.");
                    $(".val-price7").find('strong').text(filter(v7) + " руб.");
                });

                $(".calculator__select-wrapper").on("click", function (e) {
                    if ($(e.target).hasClass('calculator__select-wrapper')) {
                        open($(e.target).find('select'));
                        return false;
                    }
                });
            }
        };
    }()

    /**
     * Gallery Images
     */
    ,
        Gallery = function () {
        var $images = $('#gallery').find('.gallery__link');
        var $large = $("#gallery").find('.gallery__large').find('img');
        return {
            init: function init() {
                if (!$images.length) return false;

                $images.on('click', function () {
                    var $i = $(event.currentTarget);

                    $large.attr('src', $i.attr('href'));
                    $images.removeClass('gallery__link--active');
                    $i.addClass('gallery__link--active');

                    return false;
                });
            }
        };
    }()

    /**
     * Image Titles in Typogrpahy
     */
    ,
        ImageTitles = function () {
        var $images = $('.central.typography').find('img[title]');
        return {
            init: function init() {
                if (!$images.length) return false;

                $images.each(function (i, e) {
                    var $image = $(e);
                    var title = $image.prop('title');
                    var width = $image.width();
                    $image.after("<span class=\"img-title\" style=\"width: " + width + "px\">" + title + "</span>");
                });
            }
        };
    }()

    /**
     * Nav
     */
    ,
        Nav = function () {
        return {
            init: function init() {

                var hovered = false;
                var closeTimer;

                var $elem;
                var MenuClose = function MenuClose() {
                    var $e = $(".nav__link--has-submenu");
                    $e.next().hide();
                    $e.parent().removeClass('hovered');
                    $e.parent().next().removeClass('after-hovered');
                    hovered = false;
                };

                $(".nav__list--level1").on('mouseenter', function () {
                    clearTimeout(closeTimer);
                }).on('mouseleave', function () {
                    closeTimer = window.setTimeout(function () {
                        MenuClose();
                    }, 300);
                });

                $(".nav__link--has-submenu").on('mouseenter', function (ev) {
                    MenuClose();
                    $elem = $(ev.currentTarget);
                    $elem.next().show();
                    $elem.parent().addClass('hovered');
                    $elem.parent().next().addClass('after-hovered');

                    hovered = true;
                    clearTimeout(closeTimer);
                }).on('mouseleave', function (ev) {
                    $elem = $(ev.currentTarget);

                    closeTimer = window.setTimeout(function () {
                        if (hovered) {
                            MenuClose();
                        }
                    }, 300);
                });
            }
        };
    }()

    /**
     * Dummy Module Example
     */
    ,
        DummyModule = function () {
        return {
            init: function init() {
                // do something
            }
        };
    }();App.init();
});

function open(elem) {
    if (document.createEvent) {
        var e = document.createEvent("MouseEvents");
        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        elem[0].dispatchEvent(e);
    } else if (element.fireEvent) {
        elem[0].fireEvent("onmousedown");
    }
}
